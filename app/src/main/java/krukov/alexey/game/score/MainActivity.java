package krukov.alexey.game.score;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity implements View.OnClickListener {

    int first_score_int = 0; //Очки первой команды
    int second_score_int = 0; //Очки второй команды

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //TextView first_score = (TextView) findViewById(R.id.first_score);
        //TextView second_score = (TextView) findViewById(R.id.second_score);

        Button first_plus = (Button) findViewById(R.id.first_plus);
        Button first_minus = (Button) findViewById(R.id.first_minus);
        Button second_plus = (Button) findViewById(R.id.second_plus);
        Button second_minus = (Button) findViewById(R.id.second_minus);

        first_plus.setOnClickListener(this);
        first_minus.setOnClickListener(this);
        second_plus.setOnClickListener(this);
        second_minus.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        TextView first_score = (TextView) findViewById(R.id.first_score);
        TextView second_score = (TextView) findViewById(R.id.second_score);

        switch(view.getId()) {
            case R.id.first_plus:
                first_score_int += 1;
                first_score.setText(Integer.toString(first_score_int));
                break;
            case R.id.first_minus:
                if(first_score_int != 0) first_score_int -= 1;
                first_score.setText(Integer.toString(first_score_int));
                break;
            case R.id.second_plus:
                second_score_int += 1;
                second_score.setText(Integer.toString(second_score_int));
                break;
            case R.id.second_minus:
                if(second_score_int != 0) second_score_int -= 1;
                second_score.setText(Integer.toString(second_score_int));
                break;
        }
    }
}
